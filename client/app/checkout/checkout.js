﻿/*
 * Cart viewmodel associated with cart.html view
 */
(function(angular) {
    'use strict';

    angular.module("app").controller( 'checkout',
        ['dataservice', '$http', controller]);

    function controller(dataservice, $http) {
        var vm   = this;
        dataservice.ready().then(onReady);

        function onReady(){
            vm.cartOrder     = dataservice.cartOrder;
            vm.draftOrder    = dataservice.draftOrder;
			
			vm.orders			= getOrders();
			vm.customers		= getCustomers();
			vm.selectedCustomer = null;
			vm.placeOrder		= placeOrder;
			vm.newCustomer		= null;
		}

        function getOrders() {
            vm.orders = dataservice.getOrders()
                .then(gotOrders);
				
            function gotOrders(orders){
                vm.orders = orders;
            }
        }
		
        function getCustomers() {
            vm.isLoadingCustomers = true;
            vm.customers = dataservice.getCustomers()
                .then(gotCustomers);
				
            function gotCustomers(customers){
                vm.customers = customers;
            }
        }

        function placeOrder(type){
			
			if (type=='new')
			{
				vm.selectedCustomer = vm.newCustomer;
				vm.customers.push(dataservice.createCustomer(vm.newCustomer));
				console.log(vm.customers);
			}
			
			vm.cartOrder.customerId=vm.selectedCustomer.id;
			vm.cartOrder.customer=vm.selectedCustomer;
			vm.cartOrder.name=vm.selectedCustomer.fullName;
			vm.cartOrder.ordered=Date();
			vm.cartOrder.phone=vm.selectedCustomer.phone;
			vm.cartOrder.statusId=1;
			vm.selectedCustomer.orders.push(vm.cartOrder);
			
			console.log(vm.cartOrder);
			console.log(vm.selectedCustomer);			
			
			//saving changes through breeze in either case <-- this throws an error!
			
			/*
			dataservice.saveChanges()
				.then(saveSucceeded)
				.fail(saveFailed);
			
			function saveSucceeded()
			{
				alert('done');
			}

			function saveFailed(error) {
				var msg = 'Save failed: ' + breeze.saveErrorMessageService.getErrorMessage(error);
				error.message = msg;
				logError(msg, error);
				throw error;
			} 
			*/
			
			//sending email to the customer
			$http({
				method: 'POST',
				url: '/mail', 
				data: $.param({ useremail: vm.selectedCustomer.email, orderdamount: vm.cartOrder.itemsTotal, phone: vm.cartOrder.phone }),
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(
				function(res) {
					console.log('succes !', res.data);
				},
				function(err) {
					console.log('error...', err);
				}
			);
        }
				
	   /////////////////////
       
    }

})(this.angular);
