#Zza Node Mongo
The "Zza Node Mongo" sample app is a single page application (SPA) built with Breeze, Angular, Node, and MongoDB. Instructions to install and run follow.

## Prerequisites

- Node.js + npm
- mongodb
- Bower

##Install MongoDb database and run MongoDb server

Unzip *~/database/zza-mongo-database.zip* into the *database* directory.

Open a command / terminal window

Start mongodb server

Use mongorestore -d command to import collections into mongodb

use robomongo to check collections (https://robomongo.org/)

##Install dependencies and launch app server

Open a second command / terminal window

Navigate to the client folder, *~/node/zza-node-mongo/client*

Install the bower packages: `bower install`

Navigate to the server folder, *~/node/zza-node-mongo/server*

Install the node modules: `npm install`

Launch the app server: `node server.js`

Console output should indicate that app server started successfully and is **listening on port 3000**.

##Launch Zza in a browser

Start your browser with address [**http://localhost:3000**](http://localhost:3000)
